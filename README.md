# BSP-3.0.6

- `mkdir yocto-renesas-rzv`
- `cd yocto-renesas-rzv`
- `python3 ~/bin/repo init -u https://gitlab.com/OneKiwiTech/embedded/renesas/manifest-renesas-rz -b dunfell -m rzv2l-bsp-3.0.6.xml`
- `python3 ~/bin/repo sync -j8`
- `source poky/oe-init-build-env`
- `cp ../meta-renesas/meta-rzv2l/docs/template/conf/onekiwi-rzv2l/*.conf conf/`
- `bitbake core-image-weston`
